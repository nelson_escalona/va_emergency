 odoo.define('voltus_hide_import_btn_widget', function (require) {
    'use strict';
     var KanbanController = require('web.KanbanController');
     KanbanController.include({
            renderButtons: function ($node) {
               this._super.apply(this, arguments);

               if (this.modelName.startsWith('project.project')||this.modelName.startsWith('project.task')) {
                   if (this.$buttons){
                     this.$buttons.find('.o_button_import').hide();
                   }
               }
            }
     });
 });