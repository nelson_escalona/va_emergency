# -*- coding: utf-8 -*-
{
    'name': 'VA Emergency',
    'version': '1.1',
    'website': 'https://www.voltusautomation.com',
    'category': 'Site',
    'sequence': 11,
    'summary': 'Organize Emergency process',
    # any module necessary for this one to work correctly
    'depends': ['base', 'project'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/project_views.xml',
     ],
    'assets': {
        'web.assets_backend': [
            'va_emergency/static/src/js/voltus_row_background_color_widget.js',
            'va_emergency/static/src/js/hide_import_btn.js',
        ],
    },
}
