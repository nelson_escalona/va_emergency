# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

class ProjectTaskType(models.Model):
    _name = 'project.task.type'
    _inherit = ['project.task.type']

    # color = fields.Integer(string='Color Index')
    color = fields.Selection([('white', 'white'), ('#DCDCDC', 'gray'), ('#A9D08E', 'green'), ('#FFF2CC', 'yellow'), ('#00B0F0', 'blue'), ('#CC99FF', 'purple')], required=False, default="white")
    duedate_trigger = fields.Boolean(string="Due Date Trigger", default=False)
    due_days = fields.Integer(string="Due Days", default=30)

class Project(models.Model):
    _name = "project.project"
    _inherit = ['project.project']

    p_email_from = fields.Char(string='Email', help="These people will receive email.", index=True)
    p_mobile_from = fields.Char(string='Mobile', index=True)
    project_number = fields.Char("Project Number", index=True, required=False, track_visibility='onchange')
    parent_id = fields.Many2one('project.project', string='Parent Project', index=True)

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.p_email_from = self.partner_id.email
        self.p_mobile_from = self.partner_id.mobile


    def action_duplicate(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.project',  # this model
            'res_id': self.copy().id,  # the current wizard record
            'view_mode': 'form',
            'view_type': 'form',
            'views': [[False, 'form']],
            'context': {'form_view_initial_mode': 'edit'},
        }


class Task(models.Model):
    _name = "project.task"
    _inherit = ['project.task']

    wo_po = fields.Char(string='WO/PO #')
    sr = fields.Char(string='SR #')
    kpj = fields.Char(string='KPJ #')
    invoice_number = fields.Char(string='Invoice #')
    invoice = fields.Float(string='Invoice')
    invoice_tax = fields.Float(string='Invoice +Tax')
    service_type = fields.Selection([('Emergency', 'Emergency'), ('Maintenance', 'Maintenance'), ('Standby', 'Standby')], string="Service Type",
                                    required=False, default="Emergency")
    due_date = fields.Date(string='Due Date', index=True, copy=False, track_visibility='onchange')
    mobile_from = fields.Char(string='Mobile', index=True)
    email_from = fields.Char(string='Email', help="These people will receive email.", index=True)


    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.email_from = self.partner_id.email
        self.mobile_from = self.partner_id.mobile

    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        now = fields.Datetime.now()
        stage = self.stage_id
        if stage.duedate_trigger:
            self.due_date = (now + datetime.timedelta(stage.due_days)).date()

